# Polysun Plugin Template #

TODO: Replace this readme with your own.

## Getting started ##

* Clone this repository.
* Rename the cloned directory to your plugin's name.
* Navigate to the cloned directory.
* Delete the `.git` directory.
* Add your source code to the `src/main/java` directory.
* Add your test code to the `src/test/java` directory.
* Search this project's file contents for `TODO` tags and add your custom information.
    * [`build.gradle.kts`](./build.gradle.kts)
    * [`settings.gradle.kts`](./settings.gradle.kts)
    * [`com.velasolaris.plugin.controller.spi.ControllerPlugin`](./src/main/resources/META-INF/services/com.velasolaris.plugin.controller.spi.ControllerPlugin)

Please refer to the Polysun user manual and the [`polysun-plugin-if/README.md`](https://bitbucket.org/velasolaris/polysun-plugin-if/src/master/README.md) for plugin development documentation.

## Building your plugin ##

* In Linux or macOS, run `./gradlew` from a terminal, or double-click the `gradlew` file.
* In Windows, double-click the `gradlew.bat` file to build your plugin (Windows may hide the .bat file extension) or run `gradlew.bat` from a PowerShell session.
* The resulting Jar file can be found in the `build/libs` subdirectory of this project.
* You can also install your plugin directly using the `./gradlew install` or `gradlew.bat install` task.
  If you have installed Polysun to a non-default location, make sure to set the installation directory in the [`gradle.properties`](./gradle.properties) file.

## Using your plugin in Polysun ##

To use your plugin, copy it to the `plugins` subdirectory of the Polysun data directory.
If Polysun is running, it will be available after restarting Polysun.

## Debugging ##

Polysun uses `java.util.logging.Logger` for logging, and files are logged to the file `data.log` in the Polysun data directory.
If you use a `java.util.logging.Logger`, your plugin's logs will be added to the same log file automatically.
By default, Polysun only writes logs with a `WARNING` or `SEVERE` level to the log file.
You can change this behaviour by editing the `logger.config` file in the Polysun installation directory.
You can use the [`polysun-logger-filter`](https://bitbucket.org/velasolaris/polysun-logger-filter/) project to create a Jar file that you can add to Polysun for filtering logs.

>Note: Once your plugin has been loaded into Polysun, you must restart Polysun to load any modifications.
